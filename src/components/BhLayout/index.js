import BhMenuItem from './src/menu-item'

const components = [
  BhMenuItem
]

const install = function (Vue) {
  components.map(component => {
    Vue.component(component.name, component)
  })
}

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}

var version = '1.0.1'
var name = 'BhLayout'

export {
  version,
  name,
  install,
  BhMenuItem
}

export default {
  version,
  name,
  install,
  BhMenuItem
}
