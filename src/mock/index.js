import Mock from 'mockjs'
import UserApi from './user/users'

// user
Mock.mock(/\/userapi\/user\/list/, 'get', UserApi.list)
Mock.mock(/\/userapi\/user\/list/, 'post', UserApi.list)

export default Mock
