import Mock from 'mockjs'
import moment from 'moment'

const List = Mock.mock({
  'list|100': [{
    'id|+1': 1,
    'name': Mock.Random.cname(),
    'age|1-100': 100,
    'phone': /^1[385][1-9]\d{8}/,
    'address': Mock.mock('@county(true)'),
    'createTime': moment(Mock.Random.datetime()).valueOf(),
    'updateTime': moment(Mock.Random.datetime()).valueOf()
  }]
}).list

const users = {
  list: config => {
    console.log(config)
    return {
      code: 0,
      message: 'success',
      result: List
    }
  }
}

export default users
