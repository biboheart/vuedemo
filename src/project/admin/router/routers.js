import Wrap from '@/components/Wrap.vue'

const routers = [{
  path: '/',
  name: 'home',
  text: '首页',
  menu: false,
  redirect: '/dashboard',
  icon: 'fas fa-home',
  component: Wrap
}, {
  path: '/dashboard',
  name: 'dashboard',
  text: '仪表盘',
  menu: true,
  icon: 'fas fa-tachometer-alt',
  component: resolve => require(['../views/Dashboard'], resolve)
}, {
  path: '/user',
  name: 'user',
  text: '用户管理',
  menu: true,
  hasChildren: true,
  icon: 'fas fa-user-cog',
  component: Wrap,
  children: [{
    path: '/user/user',
    name: 'user_user',
    text: '用户列表',
    menu: true,
    component: resolve => require(['../views/user/User'], resolve)
  }]
}, {
  path: '/components',
  name: 'components',
  text: '组件',
  menu: true,
  hasChildren: true,
  icon: 'fas fa-th-large',
  component: Wrap,
  children: [{
    path: '/components/upload',
    name: 'components_upload',
    text: '文件上传',
    menu: true,
    component: resolve => require(['../views/components/Upload'], resolve)
  }]
}]

export default routers
