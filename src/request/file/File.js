// IMAGE api
import Service from '../base'
export default {
  uploadImage (file, handleProgress) {
    const formData = new FormData()
    formData.append('file', file)
    return Service.post('/zuul/image/upload', formData, {
      progress (e) {
        if (e.total > 0) {
          e.percent = e.loaded / e.total * 100
        }
        if (handleProgress) {
          handleProgress(e)
        }
      },
      onUploadProgress (e) {
        if (e.total > 0) {
          e.percent = e.loaded / e.total * 100
        }
        if (handleProgress) {
          handleProgress(e)
        }
      }
    })
  }
}
