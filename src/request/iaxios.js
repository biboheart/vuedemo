import axios from 'axios'
import utils from './utils'

const transformRequest = data => {
  if (utils.isFormData(data) ||
    utils.isArrayBuffer(data) ||
    utils.isBuffer(data) ||
    utils.isStream(data) ||
    utils.isFile(data) ||
    utils.isBlob(data) ||
    utils.isArrayBufferView(data) ||
    utils.isURLSearchParams(data)
  ) {
    return data
  }
  if (utils.isObject(data)) {
    for (var pkey in data) {
      if (data[pkey] === null || typeof (data[pkey]) === 'undefined') {
        delete data[pkey]
      }
    }
    data = utils.params(data)
    return data
  }
  return data
}

axios.defaults.transformRequest.unshift(transformRequest)

const instance = axios.create({
  baseURL: process.env.VUE_APP_SERVER_PATH || location.protocol + '//' + location.host
})

instance.interceptors.request.use(config => {
  // 可以在这里加入ajax请求前处理的业务，如加入access_token
  return config
})

instance.interceptors.response.use(response => {
  return response.data
}, error => {
  let response = error.response
  return Promise.reject(response.data)
})

export default instance
