import Service from '@/request/base'

export default {
  list (options) {
    return Service.post('/userapi/user/list', options)
  }
}
