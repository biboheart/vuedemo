module.exports = {
  pages: {
    index: 'src/project/index/main.js',
    admin: 'src/project/admin/main.js',
    login: 'src/project/login/main.js'
  },
  devServer: {
    port: 3003
  }
}
